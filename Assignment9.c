#include <stdio.h>

int main(){
    char data[100];
    FILE *fptr;

    fptr = fopen("assignment9.txt" , "w");
    fprintf(fptr , "UCSC is one of the leading institutes in Sri Lanka for computing studies.");
    fclose(fptr);

    fptr = fopen("assignment9.txt" , "r");
    fgets(data , 100 , fptr);
    printf("%s\n",data);
    fclose(fptr);

    fptr = fopen("assignment9.txt" , "a");
    fprintf(fptr , "UCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
    fclose(fptr);

    return 0;


}
